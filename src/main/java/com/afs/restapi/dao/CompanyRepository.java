package com.afs.restapi.dao;

import com.afs.restapi.pojo.Company;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: practice-spring-boot-employee-starter
 * @author: yoki
 * @create: 2023-07-18 19:38
 */
@Repository("companyRepository")
public class CompanyRepository {
    private List<Company> companies;

    public CompanyRepository(List<Company> companies) {
        this.companies = companies;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyById(int id) {
        return this.companies.stream().filter(company -> company.getId() == id).findFirst().orElse(null);
    }

    public Company addCompany(Company company) {
        company.setId(nextId());
        companies.add(company);
        return company;
    }

    public Company updateCompany(int id, Company company) {
        Company companyFound = companies.stream().filter(companyInList -> companyInList.getId() == id).findFirst().orElse(null);
        if (companyFound != null) {
            companyFound.setName(company.getName());
        }
        return companyFound;
    }

    public List<Company> getCompanyPage(int page, int size) {
        return companies.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    public void deleteCompany(int id) {
        companies.stream().filter(companyInList -> companyInList.getId() == id)
                .findFirst()
                .ifPresent(companyInList -> companies.remove(companyInList));
    }

    private int nextId() {
        int maxId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }

    public void clear(){
        companies.clear();
    }
}
