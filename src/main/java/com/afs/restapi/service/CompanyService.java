package com.afs.restapi.service;

import com.afs.restapi.dao.CompanyRepository;
import com.afs.restapi.dao.EmployeeRepository;
import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: restapi
 * @author: yoki
 * @create: 2023-07-19 21:04
 */
@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    public CompanyService() {

    }

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<Company> getAllCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(@PathVariable int id) {
        return companyRepository.getCompanyById(id);
    }

    public Company addCompany(@RequestBody Company company) {
        return companyRepository.addCompany(company);
    }

    public Company updateCompany(@PathVariable int id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    public List<Company> getCompanyPage(@RequestParam int page, @RequestParam int size) {
        return companyRepository.getCompanyPage(page, size);
    }

    public List<Employee> getEmployeesByCompanyId(@PathVariable int companyId) {
        return employeeRepository.getEmployeesByCompanyId(companyId);
    }

    public void deleteCompany(@PathVariable int id) {
        companyRepository.deleteCompany(id);
        employeeRepository.deleteEmployeesByCompanyId(id);
    }
}
