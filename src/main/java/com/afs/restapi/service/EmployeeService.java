package com.afs.restapi.service;

import com.afs.restapi.dao.EmployeeRepository;
import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.exception.IllegalStatusException;
import com.afs.restapi.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee getById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(Integer page, Integer size) {
        return employeeRepository.findByPage(page, size);
    }

    public Employee insertEmp(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new AgeErrorException();
        }
        if (employee.getAge() >= 30 && employee.getSalary() <= 20000) {
            throw new AgeNotMatchSalaryException();
        }
        employee.setActive(true);
        return employeeRepository.insert(employee);
    }

    public Employee updateEmp(Long id, Employee employee) {
        if (!employee.isActive()){
            throw new IllegalStatusException();
        }
        return employeeRepository.update(id, employee);
    }

    public void delEmp(Long id) {
        Employee employeeFound = employeeRepository.findById(id);
        employeeRepository.updateActive(employeeFound);
    }
}