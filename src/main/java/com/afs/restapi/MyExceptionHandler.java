package com.afs.restapi;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @program: restapi
 * @author: yoki
 * @create: 2023-07-19 21:55
 */
@RestControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(value = EmployeeNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse employeeNotFoundException(Exception e) {
        return new ErrorResponse("404", e.getMessage());
    }

    @ExceptionHandler(value = CompanyNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String companyNotFoundException(Exception e) {
        return e.getMessage();
    }
}
