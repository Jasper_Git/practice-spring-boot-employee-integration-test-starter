package com.afs.restapi;

import com.afs.restapi.dao.CompanyRepository;
import com.afs.restapi.dao.EmployeeRepository;
import com.afs.restapi.pojo.Company;
import com.afs.restapi.pojo.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @program: restapi
 * @author: yoki
 * @create: 2023-07-19 21:12
 */
@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private MockMvc client;

    @BeforeEach
    void setUp() {
        companyRepository.clear();
        employeeRepository.clear();
    }

    @Test //???
    void should_return_employees_in_special_company_when_perform_get_given_company_id() throws Exception {
        Employee employee1 = new Employee(1L, "zhangsan", 35, "Male", 11000, true, 1);
        Employee employee2 = new Employee(2L, "lisi", 42, "Male", 11000, true, 1);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee1);//should be employee2

        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(employee1.getId()))
                .andExpect(jsonPath("$[1].id").value(employee2.getId()));
    }

    @Test
    void should_update_specific_company_when_put_a_specific_company_given_company() throws Exception {
        Company oldCompany = new Company(1, "oldCompany");
        Company newCompany = new Company(2, "newCompany");
        companyRepository.addCompany(oldCompany);

        String oldCompanyAsString = new ObjectMapper().writeValueAsString(newCompany);

        client.perform(MockMvcRequestBuilders.put("/companies/{id}", oldCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(oldCompanyAsString))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(oldCompany.getId()))
                .andExpect(jsonPath("$.name").value(oldCompany.getName()))
                .andExpect(result -> assertEquals(companyRepository.getCompanyById(oldCompany.getId()).getName(), newCompany.getName()));
    }


    @Test
    void should_return_company_when_perform_get_given_company_id() throws Exception {
        Company company = new Company(1, "spring");
        companyRepository.addCompany(company);

        client.perform(MockMvcRequestBuilders.get("/companies/{id}", company.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(company.getId()))
                .andExpect(jsonPath("$.name").value(company.getName()));
    }

    @Test
    void should_return_specific_company_when_query_with_specific_page_and_size_given_page_and_size() throws Exception {

        companyRepository.addCompany(new Company(1, "company1"));
        companyRepository.addCompany(new Company(2, "company2"));
        companyRepository.addCompany(new Company(3, "company3"));
        companyRepository.addCompany(new Company(4, "company4"));

        client.perform(MockMvcRequestBuilders.get("/companies").param("page", "1").param("size", "2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name").value("company1"))
                .andExpect(jsonPath("$[1].name").value("company2"));
    }


    @Test
    void should_return_companies_when_getAllCompany_given_companies_in_repo() throws Exception {
        Company spring = new Company(1, "spring");
        Company thoughtworks = new Company(2, "thoughtworks");
        companyRepository.addCompany(spring);
        companyRepository.addCompany(thoughtworks);

        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(spring.getId()))
                .andExpect(jsonPath("$[0].name").value(spring.getName()))
                .andExpect(jsonPath("$[1].id").value(thoughtworks.getId()))
                .andExpect(jsonPath("$[1].name").value(thoughtworks.getName()));
    }


    @Test
    void should_return_company_when_perform_post_given_company_info() throws Exception {
        Company company = new Company(1, "spring");
        String companyAsString = new ObjectMapper().writeValueAsString(company);
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType("application/json")
                        .content(companyAsString))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value("spring"));
    }

    @Test
    void should_delete_employees_in_company_when_perform_delete_given_company_id() throws Exception {
        companyRepository.addCompany(new Company(1, "spring"));//should be changed
        companyRepository.addCompany(new Company(2, "thoughtworks"));

        employeeRepository.insert(new Employee(1L,"zhangsan",18,"male",11000,true,1));
        employeeRepository.insert(new Employee(2L,"lisi",18,"male",11000,true,1));
        employeeRepository.insert(new Employee(3L,"wangwu",18,"male",11000,true,2));
        client.perform(MockMvcRequestBuilders.delete("/companies/1"))//delete employees
                .andExpect(status().isNoContent());
    }
}

