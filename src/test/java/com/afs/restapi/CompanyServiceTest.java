//package com.afs.restapi;
//
//import com.afs.restapi.dao.CompanyRepository;
//import com.afs.restapi.pojo.Company;
//import com.afs.restapi.service.CompanyService;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.Mockito.*;
//
///**
// * @program: restapi
// * @author: yoki
// * @create: 2023-07-19 21:44
// */
//public class CompanyServiceTest {
//    private CompanyRepository companyRepository = Mockito.mock(CompanyRepository.class);
//
//    @Autowired
//    private CompanyService companyService = new CompanyService(companyRepository);
//
//    @Test
//    void should_return_company_when_insert_given_company_info() {
//        // given
//        Company company = new Company(1, "oocl");
//        when(companyRepository.addCompany(any())).thenReturn(company);
//
//        // when
//        Company actualEmployee = companyService.addCompany(company);
//        assertEquals(company, actualEmployee);
//        verify(companyRepository, times(1)).addCompany(company);
//    }
//
//    @Test
//    void should_return_company_when_get_given_company_id() {
//        // given
//        Company company = new Company(1, "oocl");
//        when(companyRepository.getCompanyById(any())).thenReturn(company);
//
//        // when
//        // then
//        Company actualEmployee = companyService.addCompany(company);
//
//        assertEquals(company, actualEmployee);
//    }
//
//}
