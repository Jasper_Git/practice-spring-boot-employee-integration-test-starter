package com.afs.restapi;

import com.afs.restapi.dao.EmployeeRepository;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.pojo.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc client;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        employeeRepository.clear();
    }

    @Test
    void should_return_employees_when_perform_get_given_employees_in_repo() throws Exception {
        // given
        employeeRepository.insert(new Employee(1L, "zhangsan", 18, "male", 2000));
        employeeRepository.insert(new Employee(2L, "lisi", 18, "female", 2000));

        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("zhangsan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(18))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("lisi"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(18));
    }

    @Test
    void should_return_employee_when_perform_get_by_id_given_employees_in_repo_and_right_id() throws Exception {
        // given
        employeeRepository.insert(new Employee(1L, "zhangsan", 18, "male", 2000));
        employeeRepository.insert(new Employee(2L, "lisi", 18, "female", 2000));

        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", 1L))
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("zhangsan"))
                .andExpect(jsonPath("$.age").isNumber());
    }

    @Test
    void should_return_employees_when_perform_get_by_gender_given_employees_in_repo_and_gender() throws Exception {
        // given
        employeeRepository.insert(new Employee(1L, "zhangsan", 18, "male", 2000));
        employeeRepository.insert(new Employee(2L, "zhaowu", 18, "male", 2000));
        employeeRepository.insert(new Employee(3L, "david", 18, "female", 2000));
        employeeRepository.insert(new Employee(4L, "lisi", 18, "female", 2000));

        //when

        //then
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "male"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("zhangsan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(18))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("zhaowu"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(18));
    }

    @Test
    void should_return_employees_when_perform_get_page_given_employees_and_page_and_size() throws Exception {
        // given
        employeeRepository.insert(new Employee(1L, "zhangsan", 18, "male", 2000));
        employeeRepository.insert(new Employee(2L, "zhaowu", 18, "male", 2000));
        employeeRepository.insert(new Employee(3L, "david", 18, "female", 2000));
        employeeRepository.insert(new Employee(4L, "lisi", 18, "female", 2000));

        // when
        //then
        client.perform(MockMvcRequestBuilders.get("/employees").param("page", "1").param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("zhangsan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(18))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("zhaowu"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(18));
    }

    @Test
    void should_return_employee_when_perform_post_given_employee_info() throws Exception {
        Employee employeeZhangsan = new Employee(1L, "zhangsan", 18, "male", 2000);
        String employeeAsString = objectMapper.writeValueAsString(employeeZhangsan);
        client.perform(MockMvcRequestBuilders.post("/employees").contentType("application/json").content(employeeAsString))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("zhangsan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(18));

        Employee employeeGot = employeeRepository.findById(1L);
        Assertions.assertEquals(employeeZhangsan, employeeGot);
    }

    @Test
    void should_return_employee_when_perform_put_given_employee() throws Exception {
        // given
        employeeRepository.insert(new Employee(1L, "zhangsan", 18, "male", 2000,true));
        employeeRepository.insert(new Employee(2L, "zhaowu", 18, "male", 2000,true));
        employeeRepository.insert(new Employee(3L, "david", 18, "female", 2000,true));
        employeeRepository.insert(new Employee(4L, "lisi", 18, "female", 2000,true));

        Employee employeeZhangsan = new Employee(1L, "zhangsan", 20, "male", 3000,true);
        String employeeAsString = objectMapper.writeValueAsString(employeeZhangsan);
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", employeeZhangsan.getId()).contentType("application/json").content(employeeAsString))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("zhangsan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(20));

        Employee employeeGot = employeeRepository.findById(1L);
        Assertions.assertEquals(employeeZhangsan, employeeGot);
    }

    @Test
    void should_can_not_find_employee_deleted_when_perform_delete_given_employee_id_to_be_delete() throws Exception {
        // given
        employeeRepository.insert(new Employee(1L, "zhangsan", 18, "male", 2000));
        employeeRepository.insert(new Employee(2L, "zhaowu", 18, "male", 2000));
        employeeRepository.insert(new Employee(3L, "david", 18, "female", 2000));
        employeeRepository.insert(new Employee(4L, "lisi", 18, "female", 2000));
        client.perform(MockMvcRequestBuilders.delete("/employees/{id}", 2))
                .andExpect(status().isNoContent());

        // when
        // then
        EmployeeNotFoundException notFoundException = Assertions.assertThrows(EmployeeNotFoundException.class, () -> employeeRepository.findById(6L));// test Handler
        assertEquals("employee id not found", notFoundException.getMessage());
    }

    @Test
    void should_404() throws Exception {
        // given
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", 100))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.code").value("404"))
                .andExpect(jsonPath("$.message").value("employee id not found"));

    }


}


