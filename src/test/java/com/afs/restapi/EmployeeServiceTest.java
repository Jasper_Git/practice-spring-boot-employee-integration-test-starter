package com.afs.restapi;

import com.afs.restapi.dao.EmployeeRepository;
import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.exception.IllegalStatusException;
import com.afs.restapi.pojo.Employee;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.*;

public class EmployeeServiceTest {
    private final EmployeeRepository employeeRepository = Mockito.mock(EmployeeRepository.class);

    private final EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_throws_age_error_exception_when_insert_employee_given_employee_aged_15_and_66() {
        // given
        Employee employeeZhangsa = new Employee(null, "zhangsan", 15, "male", 200);
        Employee employeeLisi = new Employee(null, "lisi", 66, "male", 2000);

        // when
        // then
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insertEmp(employeeZhangsa));
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insertEmp(employeeLisi));
        Mockito.verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    void should_throw_age_and_salary_not_match_error_when_insert_employee_aged_40_with_salary_15k() {
        // given
        Employee employeeZhangsa = new Employee(null, "zhangsan", 40, "male", 15000);

        // when
        // then
        Assertions.assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.insertEmp(employeeZhangsa));
        Mockito.verify(employeeRepository, Mockito.times(0)).insert(any());
    }

    @Test
    void should_return_employee_with_active_true_when_insert_employee_given_a_match_rule() {
        // given
        Employee employeeZhangsa = new Employee(null, "zhangsan", 35, "male", 35000);
        Employee employeeExpected = new Employee(1L, "zhangsan", 35, "male", 35000);
        employeeExpected.setActive(true);
        Mockito.when(employeeRepository.insert(any())).thenReturn(employeeExpected);

        // when
        Employee employeeSaved = employeeService.insertEmp(employeeZhangsa);

        // then
        Assertions.assertTrue(employeeSaved.isActive());
        Mockito.verify(employeeRepository).insert(Mockito.argThat(employeeToSave -> {
            Assertions.assertTrue(employeeToSave.isActive());
            return true;
        }));
    }

    @Test
    void should_set_employee_with_active_false_when_delete_employee_given_a_id() {
        // given
        Employee employeeExpected = new Employee(1L, "zhangsan", 35, "male", 35000);
        employeeExpected.setActive(false);
        Mockito.when(employeeRepository.findById(any())).thenReturn(employeeExpected);

        // when
        employeeService.delEmp(1L);

        // then
        Assertions.assertFalse(employeeExpected.isActive());
        Mockito.verify(employeeRepository).updateActive(argThat(employee -> {
            Assertions.assertFalse(employee.isActive());
            return true;
        }));
    }

    @Test
    void should_not_update_employee_with_active_false_when_update_employee_given_employee_info() {
        // given
        Employee employeeLeft = new Employee(1L, "zhangsan", 35, "male", 35000);
        employeeLeft.setActive(false);
        Mockito.when(employeeRepository.findById(any())).thenReturn(employeeLeft);

        //when
        //then
        Assertions.assertThrows(IllegalStatusException.class, () -> employeeService.updateEmp(employeeLeft.getId(), employeeLeft));
        Mockito.verify(employeeRepository, Mockito.times(0)).update(employeeLeft.getId(), employeeLeft);
    }
}
