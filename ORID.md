## Objective

- Today, I learned about integration testing of controllers, the three-tier architecture of SpringBoot engineering, and abstraction of business logic and unit testing of services.

## Reflective

- Today was not an easy day for me, encountering many unfamiliar technologies, such as integration testing and unit testing with Mock.

## Interpretive

- Differences between integration and unit tests:

  - Unit testing: Can be done in the developer's development environment and does not require complex deployment and configuration. They should be independent of changes in external dependencies and focus only on the internal logic of the unit under test.
  - Integration testing: It usually needs to be done in an environment closer to the real production environment and may involve real external dependencies such as databases, networks, message queues, etc. Therefore, integration testing may require additional resources and configuration to set up the test environment.

## Decisional

- Learn and practice more testing methods, including mock requests, mock databases, and more